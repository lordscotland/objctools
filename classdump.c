#include <mach-o/dyld.h>
#include <objc/runtime.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* schars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
struct item {
  const char* name;
  const char* sptr;
  union {
    Class class;
    Method method;
    Ivar ivar;
  };
};
static int cmpitems(const struct item* a,const struct item* b) {
  return strcmp(a->sptr?:"",b->sptr?:"");
}
static void printclassmethods(Class cls,char pre,FILE* fh){
  unsigned int count,i;
  Method* methods=class_copyMethodList(cls,&count);
  struct item* items=malloc(sizeof(struct item)*count);
  for (i=0;i<count;i++){
    items[i].sptr=strpbrk(items[i].name=sel_getName(
     method_getName(items[i].method=methods[i])),schars);
  }
  free(methods);
  qsort(items,count,sizeof(struct item),(void*)cmpitems);
  for (i=0;i<count;i++){
    const char* name=items[i].name;
    Method method=items[i].method;
    char* rtype=method_copyReturnType(method);
    if(!rtype){
      fprintf(fh,"  %c(?) %s\n",pre,name);
      continue;
    }
    fprintf(fh,"  %c(%c)",pre,rtype[0]);
    unsigned int nargs=method_getNumberOfArguments(method),j=2;
    while(1){
      char* ptr=strchr(name,':');
      fputc(' ',fh);
      if(ptr){
        fwrite(name,sizeof(char),++ptr-name,fh);
        if(j<nargs){
          char* atype=method_copyArgumentType(method,j++);
          fprintf(fh,"(%s)",atype);
          free(atype);
        }
        else {fputc('?',fh);}
        name=ptr;
      }
      else {
        fputs(name,fh);
        break;
      }
    }
    while(j<nargs){
      char* atype=method_copyArgumentType(method,j++);
      fprintf(fh," (%s)",atype);
      free(atype);
    }
    if(rtype[1]){fprintf(fh," => %s\n",rtype);}
    else {fputc('\n',fh);}
    free(rtype);
  }
  free(items);
}
static __attribute__((constructor)) void init() {
  int clscount=objc_getClassList(NULL,0),i;
  if(clscount>0){
    const char* imgname=_dyld_get_image_name(0);
    const char* name=strrchr(imgname,'/');
    const char* pre="/var/ea/dump-";
    const size_t prelen=strlen(pre);
    size_t namelen=strlen(name=name?name+1:imgname);
    char* buf=malloc(prelen+namelen+1);
    memcpy(memcpy(buf,pre,prelen)+prelen,name,namelen+1);
    FILE* fh=fopen(buf,"wb");
    free(buf);
    Class* classes=malloc(sizeof(Class)*clscount);
    clscount=objc_getClassList(classes,clscount);
    struct item* clsitems=malloc(sizeof(struct item)*clscount);
    for (i=0;i<clscount;i++){
      clsitems[i].sptr=strpbrk(clsitems[i].name=class_getName(
       clsitems[i].class=classes[i]),schars);
    }
    free(classes);
    qsort(clsitems,clscount,sizeof(struct item),(void*)cmpitems);
    for (i=0;i<clscount;i++){
      Class cls=clsitems[i].class,pcls=cls;
      while(pcls){
        if(pcls!=cls){fputc(0,fh);}
        fputs(class_getName(pcls),fh);
        pcls=class_getSuperclass(pcls);
      }
      fputs(":\n",fh);
      unsigned int count,i;
      Ivar* ivars=class_copyIvarList(cls,&count);
      struct item* items=malloc(sizeof(struct item)*count);
      for (i=0;i<count;i++){
        items[i].sptr=strpbrk(items[i].name=ivar_getName(ivars[i]),schars);
        items[i].ivar=ivars[i];
      }
      free(ivars);
      qsort(items,count,sizeof(struct item),(void*)cmpitems);
      for (i=0;i<count;i++){
        Ivar ivar=items[i].ivar;
        const char* vtype=ivar_getTypeEncoding(ivar);
        fprintf(fh,"  .(%c)%s [%+ld]",vtype[0],items[i].name,ivar_getOffset(ivar));
        if(vtype[1]){fprintf(fh," => %s\n",vtype);}
        else {fputc('\n',fh);}
      }
      free(items);
      printclassmethods(object_getClass((id)cls),'+',fh);
      printclassmethods(cls,'-',fh);
    }
    free(clsitems);
    fclose(fh);
  }
  exit(128);
}
