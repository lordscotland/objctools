ARCHS = arm64

include theos/makefiles/common.mk

LIBRARY_NAME += classdump
classdump_FILES = classdump.c
classdump_LDFLAGS = -lobjc

LIBRARY_NAME += decrypt
decrypt_FILES = decrypt.c

include $(THEOS_MAKE_PATH)/library.mk

TOOL_NAME += link
link_FILES = link.c

include $(THEOS_MAKE_PATH)/tool.mk

all:: dump_symbols
dump_symbols: symbols.c; $(CC) -o $@ $<
