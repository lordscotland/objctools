#include <ctype.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

#define SELECT(S,field,var,i64) (i64?((struct S##_64*)(var))->field:((struct S*)(var))->field)
struct class_t {
  uint32_t isa;
  uint32_t superclass;
  uint32_t cache;
  uint32_t vtable;
  uint32_t _data;
};
struct class_ro_t {
  uint32_t flags;
  uint32_t instanceStart;
  uint32_t instanceSize;
  uint32_t ivarLayout;
  uint32_t name;
  uint32_t baseMethods;
  uint32_t baseProtocols;
  uint32_t ivars;
  uint32_t weakIvarLayout;
  uint32_t baseProperties;
};
struct category_t {
  uint32_t name;
  uint32_t cls;
  uint32_t instanceMethods;
  uint32_t classMethods;
  uint32_t protocols;
  uint32_t instanceProperties;
};
struct method_list_t {
  uint32_t _entsize;
  uint32_t count;
  struct {
    uint32_t name;
    uint32_t types;
    uint32_t imp;
  } methods[];
};
struct class_t_64 {
  uint64_t isa;
  uint64_t superclass;
  uint64_t cache;
  uint64_t vtable;
  uint64_t _data;
};
struct class_ro_t_64 {
  uint32_t flags;
  uint32_t instanceStart;
  uint32_t instanceSize;
  uint32_t reserved;
  uint64_t ivarLayout;
  uint64_t name;
  uint64_t baseMethods;
  uint64_t baseProtocols;
  uint64_t ivars;
  uint64_t weakIvarLayout;
  uint64_t baseProperties;
};
struct category_t_64 {
  uint64_t name;
  uint64_t cls;
  uint64_t instanceMethods;
  uint64_t classMethods;
  uint64_t protocols;
  uint64_t instanceProperties;
};
struct method_list_t_64 {
  uint32_t _entsize;
  uint32_t count;
  struct {
    uint64_t name;
    uint64_t types;
    uint64_t imp;
  } methods[];
};
//#include <launch-cache/dyld_cache_format.h>
struct dyld_cache_header {
  char magic[16];
  uint32_t mappingOffset;
  uint32_t mappingCount;
  uint32_t imagesOffset;
  uint32_t imagesCount;
  uint64_t dyldBaseAddress;
  uint64_t codeSignatureOffset;
  uint64_t codeSignatureSize;
  uint64_t slideInfoOffset;
  uint64_t slideInfoSize;
  uint64_t localSymbolsOffset;
  uint64_t localSymbolsSize;
  uint8_t uuid[16];
};
struct dyld_cache_mapping_info {
  uint64_t address;
  uint64_t size;
  uint64_t fileOffset;
  uint32_t maxProt;
  uint32_t initProt;
};
struct dyld_cache_image_info {
  uint64_t address;
  uint64_t modTime;
  uint64_t inode;
  uint32_t pathFileOffset;
  uint32_t pad;
};
struct dyld_cache_local_symbols_info {
  uint32_t nlistOffset;
  uint32_t nlistCount;
  uint32_t stringsOffset;
  uint32_t stringsSize;
  uint32_t entriesOffset;
  uint32_t entriesCount;
};
struct dyld_cache_local_symbols_entry {
  uint32_t dylibOffset;
  uint32_t nlistStartIndex;
  uint32_t nlistCount;
};
//#include <mach-o/nlist.h>
#define N_STAB 0xe0
#define N_TYPE 0x0e
#define N_SECT 0xe
struct nlist {
  union {
    uint32_t n_strx;
  } n_un;
  uint8_t n_type;
  uint8_t n_sect;
  uint16_t n_desc;
  uint32_t n_value;
};
struct nlist_64 {
  union {
    uint32_t n_strx;
  } n_un;
  uint8_t n_type;
  uint8_t n_sect;
  uint16_t n_desc;
  uint64_t n_value;
};
//#include <mach-o/fat.h>
#define FAT_MAGIC 0xcafebabe
struct fat_header {
  uint32_t magic;
  uint32_t nfat_arch;
};
struct fat_arch {
  int32_t cputype;
  int32_t cpusubtype;
  uint32_t offset;
  uint32_t size;
  uint32_t align;
};
//#include <mach-o/loader.h>
#define MH_MAGIC 0xfeedface
struct mach_header {
  uint32_t magic;
  int cputype;
  int cpusubtype;
  uint32_t filetype;
  uint32_t ncmds;
  uint32_t sizeofcmds;
  uint32_t flags;
};
#define MH_MAGIC_64 0xfeedfacf
struct mach_header_64 {
  uint32_t magic;
  int cputype;
  int cpusubtype;
  uint32_t filetype;
  uint32_t ncmds;
  uint32_t sizeofcmds;
  uint32_t flags;
  uint32_t reserved;
};
#define LC_UUID 0x1b
struct load_command {
  uint32_t cmd;
  uint32_t cmdsize;
};
#define LC_SEGMENT 0x1
struct segment_command {
  uint32_t cmd;
  uint32_t cmdsize;
  char segname[16];
  uint32_t vmaddr;
  uint32_t vmsize;
  uint32_t fileoff;
  uint32_t filesize;
  int maxprot;
  int initprot;
  uint32_t nsects;
  uint32_t flags;
};
struct section {
  char sectname[16];
  char segname[16];
  uint32_t addr;
  uint32_t size;
  uint32_t offset;
  uint32_t align;
  uint32_t reloff;
  uint32_t nreloc;
  uint32_t flags;
  uint32_t reserved1;
  uint32_t reserved2;
};
#define LC_SEGMENT_64 0x19
struct segment_command_64 {
  uint32_t cmd;
  uint32_t cmdsize;
  char segname[16];
  uint64_t vmaddr;
  uint64_t vmsize;
  uint64_t fileoff;
  uint64_t filesize;
  int maxprot;
  int initprot;
  uint32_t nsects;
  uint32_t flags;
};
struct section_64 {
  char sectname[16];
  char segname[16];
  uint64_t addr;
  uint64_t size;
  uint32_t offset;
  uint32_t align;
  uint32_t reloff;
  uint32_t nreloc;
  uint32_t flags;
  uint32_t reserved1;
  uint32_t reserved2;
  uint32_t reserved3;
};

struct region {
  const char* fptr;
  const char* lc;
  uint64_t vmaddr,vmsize;
};
static struct region* dsc_create_map(const char* file) {
  const struct dyld_cache_header* dch=(const void*)file;
  const struct dyld_cache_mapping_info* infoptr=(const void*)(file+dch->mappingOffset);
  uint32_t count=dch->mappingCount;
  struct region* mapbuf=malloc(count*sizeof(*mapbuf)+sizeof(void*));
  struct region* mapptr=mapbuf;
  for (;count;--count,++infoptr){
    mapptr->fptr=file+infoptr->fileOffset;
    mapptr->lc=NULL;
    mapptr->vmaddr=infoptr->address;
    mapptr->vmsize=infoptr->size;
    mapptr++;
  }
  mapptr->fptr=NULL;
  return mapbuf;
}
static struct region* obj_create_map(const char* mh,const uint8_t** uuidptr,int is64) {
  const char* lc=mh+(is64?sizeof(struct mach_header_64):sizeof(struct mach_header));
  const uint32_t segcmd=is64?LC_SEGMENT_64:LC_SEGMENT;
  uint32_t count=SELECT(mach_header,ncmds,mh,is64);
  struct region* mapbuf=malloc(count*sizeof(*mapbuf)+sizeof(void*));
  struct region* mapptr=mapbuf;
  for (;count;--count,lc+=((struct load_command*)lc)->cmdsize){
    uint32_t cmd=((struct load_command*)lc)->cmd;
    if(cmd==segcmd){
      if(SELECT(segment_command,filesize,lc,is64)){
        mapptr->fptr=mh+SELECT(segment_command,fileoff,lc,is64);
        mapptr->lc=lc;
        mapptr->vmaddr=SELECT(segment_command,vmaddr,lc,is64);
        mapptr->vmsize=SELECT(segment_command,vmsize,lc,is64);
        mapptr++;
      }
    }
    else if(cmd==LC_UUID){
      if(uuidptr){*uuidptr=lc+sizeof(struct load_command);}
    }
  }
  mapptr->fptr=NULL;
  return realloc(mapbuf,(char*)mapptr-(char*)mapbuf+sizeof(void*))?:mapbuf;
}
static const char* lookup_addr(const struct region* mapptr,uint64_t vmaddr) {
  for (;mapptr->fptr;mapptr++){
    uint64_t vmoff=mapptr->vmaddr;
    if(vmaddr>=vmoff && ((vmoff=vmaddr-vmoff)<mapptr->vmsize)){
      return mapptr->fptr+vmoff;
    }
  }
  return NULL;
}
static const char* lookup_class(const struct region* filemap,uint64_t vmaddr,const char** infoptr,const char** mlistptr,int is64) {
  const char* info=lookup_addr(filemap,vmaddr);
  if(!info){return NULL;}
  if(infoptr){*infoptr=info;}
  info=lookup_addr(filemap,SELECT(class_t,_data,info,is64)&~(is64?7:3));
  if(!info){return NULL;}
  if(mlistptr){*mlistptr=lookup_addr(filemap,SELECT(class_ro_t,baseMethods,info,is64));}
  return lookup_addr(filemap,SELECT(class_ro_t,name,info,is64))?:"?";
}
static void print_methods(const struct region* filemap,const char pre,const char* cname,const char* mlist,int is64,FILE* fh) {
  const uint32_t count=SELECT(method_list_t,count,mlist,is64);
  for (uint32_t i=0;i<count;i++){
    const uint64_t vmaddr=SELECT(method_list_t,methods[i].imp,mlist,is64);
    if(!vmaddr){continue;}
    const char* mname=lookup_addr(filemap,SELECT(method_list_t,methods[i].name,mlist,is64));
    if(!mname){fprintf(stderr,"W: !method(0x%lx).name\n",vmaddr);continue;}
    fprintf(fh,"0x%lx:%c[%s %s]\n",vmaddr,pre,cname,mname);
  }
}
static FILE* create_output_file(const struct region* filemap,const char* mh,const char* tpath) {
  int is64;
  uint32_t magic=*(uint32_t*)mh;
  switch(magic){
    case MH_MAGIC_64:is64=1;break;
    case MH_MAGIC:is64=0;break;
    default:fprintf(stderr,"E: magic=%08X\n",magic);return NULL;
  }
  const uint8_t* uuid=NULL;
  struct region* objmap=obj_create_map(mh,&uuid,is64);
  if(!filemap){filemap=objmap;}
  const char* tn=strrchr(tpath,'/');
  const size_t tnlen=strlen(tn=tn?tn+1:tpath);
  char* fnbuf=malloc(tnlen+1+36+1);
  char* fnptr=memcpy(fnbuf,tn,tnlen)+tnlen;
  *fnptr++='.';
  for (int i=0;i<16;i++){
    if(i==4 || i==6 || i==8 || i==10){*fnptr++='-';}
    if(uuid){fnptr+=sprintf(fnptr,"%02X",uuid[i]);}
    else {*fnptr++=*fnptr++='X';}
  }
  *fnptr=0;
  printf("> %s\n",fnbuf);
  FILE* fh=fopen(fnbuf,"wx");
  free(fnbuf);
  if(fh){
    const size_t lcsize=is64?sizeof(struct segment_command_64):sizeof(struct segment_command);
    const size_t sectsize=is64?sizeof(struct section_64):sizeof(struct section);
    const int ptrsize=is64?sizeof(uint64_t):sizeof(uint32_t);
    for (const struct region* mapptr=objmap;mapptr->fptr;mapptr++){
      const char* lc=mapptr->lc;
      if(!lc || strncmp(SELECT(segment_command,segname,lc,is64),"__DATA",16)){continue;}
      uint32_t nsects=SELECT(segment_command,nsects,lc,is64);
      for (lc+=lcsize;nsects;--nsects,lc+=sectsize){
        const char* sectname=SELECT(section,sectname,lc,is64);
        enum {_classrefs,_catrefs,_selrefs} secttype;
        if(strncmp(sectname,"__objc_classlist",16)==0){secttype=_classrefs;}
        else if(strncmp(sectname,"__objc_catlist",16)==0){secttype=_catrefs;}
        else if(filemap!=objmap && strncmp(sectname,"__objc_selrefs",16)==0){secttype=_selrefs;}
        else {continue;}
        const char* ptrref=lookup_addr(filemap,SELECT(section,addr,lc,is64));
        if(!ptrref){continue;}
        const char* ptrend=ptrref+SELECT(section,size,lc,is64);
        switch(secttype){
          case _classrefs:for (;ptrref<ptrend;ptrref+=ptrsize){
            const uint64_t vmaddr=is64?*(uint64_t*)ptrref:*(uint32_t*)ptrref;
            const char* info=NULL;
            const char* mlist;
            const char* cname=lookup_class(filemap,vmaddr,&info,&mlist,is64);
            if(!cname){fprintf(stderr,"W: !class(0x%lx)\n",vmaddr);}
            else if(mlist){print_methods(filemap,'-',cname,mlist,is64,fh);}
            if(info){
              cname=lookup_class(filemap,SELECT(class_t,isa,info,is64),NULL,&mlist,is64);
              if(!cname){fprintf(stderr,"W: !class(0x%lx).isa\n",vmaddr);}
              else if(mlist){print_methods(filemap,'+',cname,mlist,is64,fh);}
            }
          } break;
          case _catrefs:for (;ptrref<ptrend;ptrref+=ptrsize){
            const uint64_t vmaddr=is64?*(uint64_t*)ptrref:*(uint32_t*)ptrref;
            const char* info=lookup_addr(filemap,vmaddr);
            if(!info){fprintf(stderr,"W: !category(0x%lx)\n",vmaddr);continue;}
            char cnamebuf[20];
            const char* cname=lookup_class(filemap,SELECT(category_t,cls,info,is64),NULL,NULL,is64);
            if(!cname){
              sprintf(cnamebuf,"0x%lx",vmaddr);
              cname=(const char*)cnamebuf;
            }
            const char* cdesc=lookup_addr(filemap,SELECT(category_t,name,info,is64));
            char* label;
            if(cdesc){
              const size_t cname_len=strlen(cname);
              const size_t cdesc_len=strlen(cdesc);
              char* ptr=label=malloc(cname_len+cdesc_len+3);
              ptr=memcpy(ptr,cname,cname_len)+cname_len;
              *ptr++='(';
              ptr=memcpy(ptr,cdesc,cdesc_len)+cdesc_len;
              *ptr++=')';
              *ptr++=0;
            }
            else {label=NULL;}
            const char* mlist;
            mlist=lookup_addr(filemap,SELECT(category_t,instanceMethods,info,is64));
            if(mlist){print_methods(filemap,'-',label?:cname,mlist,is64,fh);}
            mlist=lookup_addr(filemap,SELECT(category_t,classMethods,info,is64));
            if(mlist){print_methods(filemap,'+',label?:cname,mlist,is64,fh);}
            if(label){free(label);}
          } break;
          case _selrefs:for (;ptrref<ptrend;ptrref+=ptrsize){
            const uint64_t vmaddr=is64?*(uint64_t*)ptrref:*(uint32_t*)ptrref;
            if(lookup_addr(objmap,vmaddr)){continue;}
            const char* selname=lookup_addr(filemap,vmaddr);
            if(selname){fprintf(fh,"0x%lx:%s\n",vmaddr,selname);}
            else {fprintf(stderr,"W: !selector(0x%lx)\n",vmaddr);}
          } break;
        }
      }
    }
  }
  else {perror("E[fopen]");}
  free(objmap);
  return fh;
}
static int offsetcmp(const void* A,const void* B) {
  return ((const struct dyld_cache_local_symbols_entry*)A)->dylibOffset
   -((const struct dyld_cache_local_symbols_entry*)B)->dylibOffset;
}
int main(int argc,char** argv) {
  if(argc<2){
    fprintf(stderr,"Usage: %s <filename> [<dsc_search_string>]\n",argv[0]);
    return 1;
  }
  int status=-1;
  const char* path=argv[1];
  int fd=open(path,O_RDONLY);
  if(fd==-1){return status;}
  do {
    struct stat stbuf;
    if(fstat(fd,&stbuf)){break;}
    const char* file=mmap(NULL,stbuf.st_size,PROT_READ,MAP_PRIVATE,fd,0);
    if(file==MAP_FAILED){break;}
    status=0;
    do {
      if(*(uint64_t*)file==*(uint64_t*)"dyld_v1 "){
        char* pathmatch=(2<argc)?argv[2]:NULL;
        const int is64=memcmp(file+13,"64",3)==0;
        const int nlistsize=is64?sizeof(struct nlist_64):sizeof(struct nlist);
        const struct dyld_cache_header* dch=(const void*)file;
        const struct dyld_cache_local_symbols_info* symbols=(const void*)(file+dch->localSymbolsOffset);
        const char* nlists=(const char*)symbols+symbols->nlistOffset;
        const char* strings=(const char*)symbols+symbols->stringsOffset;
        struct dyld_cache_local_symbols_entry* entries;
        const size_t entriessize=symbols->entriesCount*sizeof(*entries);
        qsort(entries=memcpy(malloc(entriessize),(const char*)symbols
         +symbols->entriesOffset,entriessize),symbols->entriesCount,sizeof(*entries),offsetcmp);
        struct region* filemap=dsc_create_map(file);
        const struct dyld_cache_image_info* imageptr=(const void*)(file+dch->imagesOffset);
        const struct dyld_cache_image_info* imageend=imageptr+dch->imagesCount;
        for (;imageptr<imageend;imageptr++){
          const char* path=file+imageptr->pathFileOffset;
          if(pathmatch && !strstr(path,pathmatch)){continue;}
          puts(path);
          const uint64_t vmaddr=imageptr->address;
          const char* mh=lookup_addr(filemap,vmaddr);
          if(!mh){fprintf(stderr,"W: !image(0x%lx)\n",vmaddr);continue;}
          FILE* fh=create_output_file(filemap,mh,path);
          if(!fh){continue;}
          const struct dyld_cache_local_symbols_entry* entry=bsearch(
           &(struct dyld_cache_local_symbols_entry){.dylibOffset=mh-file},
           entries,symbols->entriesCount,sizeof(*entry),offsetcmp);
          if(entry){
            const char* nlistptr=nlists+entry->nlistStartIndex*nlistsize;
            const char* nlistend=nlistptr+entry->nlistCount*nlistsize;
            for (;nlistptr<nlistend;nlistptr+=nlistsize){
              const uint32_t n_strx=SELECT(nlist,n_un.n_strx,nlistptr,is64);
              if(!n_strx){continue;}
              const uint8_t n_type=SELECT(nlist,n_type,nlistptr,is64);
              if(n_type&N_STAB || (n_type&N_TYPE)!=N_SECT){continue;}
              fprintf(fh,"0x%lx:%s\n",SELECT(nlist,n_value,nlistptr,is64),strings+n_strx);
            }
          }
          fclose(fh);
        }
        free(filemap);
        free(entries);
      }
      else if(__builtin_bswap32(*(uint32_t*)file)==FAT_MAGIC){
        const struct fat_header* Fh=(const void*)file;
        const struct fat_arch* archptr=(const void*)(file+sizeof(*Fh));
        const struct fat_arch* archend=archptr+__builtin_bswap32(Fh->nfat_arch);
        for (;archptr<archend;archptr++){
          FILE* fh=create_output_file(NULL,file+__builtin_bswap32(archptr->offset),path);
          if(fh){fclose(fh);}
        }
      }
      else {
        FILE* fh=create_output_file(NULL,file,path);
        if(fh){fclose(fh);}
        else {status=-1;}
      }
    } while(0);
    munmap((void*)file,stbuf.st_size);
  } while(0);
  close(fd);
  return status;
}
