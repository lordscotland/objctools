#!/usr/bin/perl

use strict;

my $dir='dump';
if(-d $dir){die("[$dir] already exists\n");}
mkdir($dir);
my $fh;
while(<>){
  if(/^([^\s\0:][^\0:]+)/){open($fh,'>',"$dir/$1");}
  print $fh $_;
}
