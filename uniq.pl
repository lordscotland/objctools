#!/usr/bin/perl

use strict;

my ($fn)=@ARGV;
my %strings;
open(my $fh,'<',$fn);
while(<$fh>){
  chomp;
  my ($addr,$string)=split(':',$_,2);
  $addr=hex($addr);
  if(exists $strings{$addr}){$strings{$addr}->{$string}=1;}
  else {$strings{$addr}={$string=>1};}
}
close($fh);
foreach my $addr (sort {$a<=>$b} keys(%strings)){
  print sprintf('0x%x:',$addr).join('|',sort keys(%{$strings{$addr}}))."\n";
}
